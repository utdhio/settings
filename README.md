## RaspberryPi Setting

ラズパイの設定について

### 0. 前準備

* PCにSettingsレポジトリをクローン

```bash
git clone https://gitlab.com/cansat_ut_2021/settings
```

* Raspberry Pi Imagerをインストール
* MicroSDをPCに挿してマウントする
* Raspberry Pi Imagerを起動
    * CHOOSE OS->Raspberry Pi OS(32-bit)
    * CHOOSE STORAGE->MicroSDを選択
* WRITEを押す
* OSが書き込まれるまで暫く待つ
* MicroSDを再びマウント
    * 自動でアンマウントされてなければそのまま
* MicroSD初期設定

```bash
./raspbian_init /media/flap1/boot // bootディレクトリを指定
```

* MicroSDアンマウントしてラズパイに挿す

### 1. sshするまで

* 3つの端子のうち"PWR IN"の端子から給電する. microbの充電器でもPCからUSB給電でもよい.
* ラズパイ内で初期化処理をしているので暫く待つ
* PCを`raspbian/wpa_supplicant.conf`で設定した接続先に繋ぐ
* 以前ラズパイに鍵認証で接続したことがある場合は以下

```bash
ssh-keygen -f "/home/xxx/.ssh/known_hosts" -R "raspberrypi.local"
```

* PCから以下のコマンドでラズパイにsshする

```bash
ssh pi@raspberrypi.local
```

* 指示通りに入力
    * Are you sure you want to continue connecting (yes/no)? ->yes
    * pi@raspberrypi.local's password: ->raspberry(初期パスワード)

### 2. ラズパイで初期設定

* 設定ツール起動

```bash
sudo raspi-config
```

* 順を追って設定する ※今後場所が変わる可能性あり
    * `8 Update`: まずraspi-configの更新をする. 終わるのを待つ
    * `6 Advanced Options`
        * `A1 Expand Filesystem`: これによりmicroSDの全容量を使えるようになる
    * `1 System Options`
        * `S3 Password`: パスワードを変更する. `dhio`としておく
        * `S4 Hostname`: ホスト名(pi@xxx.localのxxx)を変更する. ここでは`sun`とする
        * `S5 Boot / Auto Login`: 起動時の設定. あとでユーザー名を変更するために`Console`にしておく.
    * `3 Interface Options`
        * `P1 Camera`: 使うときはenabledにする
        * `P2 SSH`: 使うときはenabledにする. すでになっている
        * `P3 VNC`: 使うときはenabledにする. bluetoothの接続をGUIで見れて便利なので使っている
        * `P4 SPI`: 使うときはenabledにする. 使う
        * `P6 Serial Port`: 使うときはenabledにする. login shellはdisabledにしてinterfaceはenabledにする.
        * `P8 Remote GPIO`: 使うときはenabledにする. pigpioとか使う時必要. ラズパイからUART使わずにlidar読むとき使った
    * `5 Localisation Options`
        * `L1 Locale`: `ja_JP.UTF-8 UTF-8`をスペースキーで選択. Default localeを`ja_JP.UTF-8`とする. 少し時間がかかる
        * `L2 TimeZone`: `Asia`->`Tokyo`
        * `L4 WLAN Country`: `JP Japan`
* 完了したらescapeキーでraspi-configから抜ける
* 再起動

```bash
sudo reboot
```

* 再起動を待ってsshする. ホスト名とパスワードを変えたので注意

```bash
ssh pi@sun.local # pass: dhio
```

* gitインストールしてSettingsレポジトリクローン

```bash
sudo apt install git -y
git clone https://gitlab.com/cansat_ut_2021/settings
cd settings
```

* 諸々更新して再起動. 勝手に抜ける

```bash
source ./raspbian_update
```

* PCから公開鍵登録（既にPCで秘密鍵と公開鍵(~/.ssh/raspi.pubとする)を持っているとする）

```bash
ssh-copy-id -i ~/.ssh/raspi.pub pi@spare.local
```

### 3. ユーザー名変更

* ユーザー名を変更する. 以下順に実行

```bash
bash settings/raspbian_change_user/change_user_1
```

* パスワードを聞かれるので`tmp`とする
* 勝手に抜けるので再びsshする. ユーザー名とパスワードに注意

```bash
ssh tmp@sun.local // pass: tmp
```

* 次段階

```bash
bash /home/pi/settings/raspbian_change_user/change_user_2
```

* 勝手に抜けるので再びsshする. ユーザー名に注意

```bash
ssh dhio@sun.local
```

* `sudo raspi-config`で設定を開き, `1 System Options`->`S5 Boot / Auto Login`を`Desktop`にする
* 最後の段階. パスワードは変えたかったら変える

```bash
bash settings/raspbian_change_user/change_user_3
```

* ユーザー名は変更されたので, その他の細かい設定を一括してやる. 時間かかるので暫く待つ. なんか聞かれたら何も入力せずにエンター押す. `git clone`の際にパスワードを聞かれるので答える.

```bash
source ./raspbian_setup
```

* VNC使えるようにするため`sudo vim /boot/config.txt`で`#hdmi_force_hotplug=1`のコメントアウトを外す

## Ubuntu Setting

### 0. 前準備

* PCにSettingsレポジトリをクローン

```bash
git clone https://gitlab.com/cansat_ut_2021/settings
```

* Raspberry Pi Imagerをインストール
* MicroSDをPCに挿してマウントする
* Raspberry Pi Imagerを起動
    * CHOOSE OS->Other general purpose OS->Ubuntu->Ubuntu Server 20.04(64bit)
    * CHOOSE STORAGE->MicroSDを選択
* WRITEを押す
* OSが書き込まれるまで暫く待つ
* MicroSDを再びマウント
    * 自動でアンマウントされてなければそのまま
* MicroSD初期設定

```bash
./ubuntu_init /media/flap1/system-boot // system-bootディレクトリを指定
```

* MicroSDアンマウントしてラズパイに挿す

### 1. sshするまで

* Type-Cの端子から給電する. Type-Cの充電器でもPCからUSB給電でもよい.
* ラズパイ内で初期化処理をしているので暫く待つ
* PCを`settings/ubuntu/network-config`で設定した接続先に繋ぐ
* ラズパイのipアドレスをPCから以下で取得する. `Nmap scan report for ubuntu (192.168.128.145)`みたいに取得できるまで繰り返す

```bash
./get_raspi_ip
```

* PCから以下のコマンドでラズパイにsshする(ipアドレスは上で取得したもの)

```bash
ssh ubuntu@192.168.128.145
```

* 指示通りに入力(最後勝手に抜ける)
    * Are you sure you want to continue connecting (yes/no)? -> yes
    * ubuntu@192.168.128.145's password: -> ubuntu
    * Current password: -> ubuntu
    * New password: -> 新しいパスワード(8桁以上英数)

### 2. ラズパイで初期設定

* sshしてからSettingsレポジトリクローン

```bash
ssh ubuntu@192.168.128.145
git clone https://gitlab.com/cansat_ut_2021/settings
cd settings
```

* ホスト名変更など.

```bash
# waiting for cache lockの後実行される
sudo apt update -y
sudo apt upgrade -y

# ホスト名変更(jupiterとする)
hostnamectl set-hostname jupiter

# ホスト名.localでsshできるように
sudo apt install avahi-daemon -y

sudo reboot
```

### 3. ユーザー名変更

* sshしてからユーザー名を変更する. 以下順に実行

```bash
ssh ubuntu@jupiter.local
bash settings/ubuntu_change_user/change_user_1
# New password -> "tmp"
exit
```

* 再びssh

```bash
ssh tmp@jupiter.local # pass: tmp
```

* 次段階

```bash
bash /home/ubuntu/settings/ubuntu_change_user/change_user_2
exit
```

* 再びssh

```bash
ssh dhio@jupiter.local
```

* 最後

```bash
bash settings/ubuntu_change_user/change_user_3
```

* 一度抜けてPCから公開鍵登録（既にPCで秘密鍵と公開鍵(~/.ssh/raspi.pubとする)を持っているとする）

```bash
ssh-copy-id -i ~/.ssh/raspi.pub dhio@jupiter.local
```

* ユーザー名は変更されたので, その他の細かい設定を一括してやる. 時間かかるので暫く待つ. `git clone/sudo`の際にパスワードを聞かれるので答える.

```bash
source ./settings/ubuntu_setup
```

### 4. zram

* 疑似メモリ拡張

```bash
sudo apt install zram-config
sudo vim /usr/bin/init-zram-swapping
# #{NRDEVICES}) * 1024 * 5)) の`* 5`を追加
# `echo $mem > /sys/block/zram${DEVNUMBER}/disksize` の上に `echo zstd > /sys/block/zram${DEVNUMBER}/comp_algorithm` を追加
sudo reboot
```

### 5. camera

* `/boot/firmware/config.txt`に以下を追記

```txt
start_x=1
```

* からの再起動

```bash
sudo reboot
```
